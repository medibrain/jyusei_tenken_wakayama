﻿partial class UserControlTenken
{
    /// <summary> 
    /// 必要なデザイナー変数です。
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// 使用中のリソースをすべてクリーンアップします。
    /// </summary>
    /// <param name="disposing">マネージ リソースが破棄される場合 true、破棄されない場合は false です。</param>
    protected override void Dispose(bool disposing)
    {
        if (disposing && (components != null))
        {
            components.Dispose();
        }
        base.Dispose(disposing);
    }

    #region コンポーネント デザイナーで生成されたコード

    /// <summary> 
    /// デザイナー サポートに必要なメソッドです。このメソッドの内容を 
    /// コード エディターで変更しないでください。
    /// </summary>
    private void InitializeComponent()
    {
            this.panel1 = new System.Windows.Forms.Panel();
            this.comboBox = new System.Windows.Forms.ComboBox();
            this.checkBoxStamp = new System.Windows.Forms.CheckBox();
            this.button1 = new System.Windows.Forms.Button();
            this.buttonNext = new System.Windows.Forms.Button();
            this.buttonPrev = new System.Windows.Forms.Button();
            this.textBoxBillMonth = new System.Windows.Forms.TextBox();
            this.labelCount = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.printDialog1 = new System.Windows.Forms.PrintDialog();
            this.printDocument1 = new System.Drawing.Printing.PrintDocument();
            this.userControlImage1 = new UserControlImage();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.comboBox);
            this.panel1.Controls.Add(this.checkBoxStamp);
            this.panel1.Controls.Add(this.button1);
            this.panel1.Controls.Add(this.buttonNext);
            this.panel1.Controls.Add(this.buttonPrev);
            this.panel1.Controls.Add(this.textBoxBillMonth);
            this.panel1.Controls.Add(this.labelCount);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(562, 37);
            this.panel1.TabIndex = 0;
            // 
            // comboBox
            // 
            this.comboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox.FormattingEnabled = true;
            this.comboBox.Items.AddRange(new object[] {
            "確定済",
            "国保画像",
            "全画像"});
            this.comboBox.Location = new System.Drawing.Point(301, 11);
            this.comboBox.Name = "comboBox";
            this.comboBox.Size = new System.Drawing.Size(74, 20);
            this.comboBox.TabIndex = 5;
            // 
            // checkBoxStamp
            // 
            this.checkBoxStamp.AutoSize = true;
            this.checkBoxStamp.Checked = true;
            this.checkBoxStamp.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxStamp.Location = new System.Drawing.Point(439, 7);
            this.checkBoxStamp.Name = "checkBoxStamp";
            this.checkBoxStamp.Size = new System.Drawing.Size(92, 28);
            this.checkBoxStamp.TabIndex = 4;
            this.checkBoxStamp.Text = "印字スタンプ\r\n(参考申請書)";
            this.checkBoxStamp.UseVisualStyleBackColor = true;
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("MS UI Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.button1.Location = new System.Drawing.Point(381, 5);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(58, 28);
            this.button1.TabIndex = 3;
            this.button1.Text = "印刷";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // buttonNext
            // 
            this.buttonNext.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.buttonNext.Location = new System.Drawing.Point(156, 6);
            this.buttonNext.Name = "buttonNext";
            this.buttonNext.Size = new System.Drawing.Size(39, 28);
            this.buttonNext.TabIndex = 2;
            this.buttonNext.Text = "＞";
            this.buttonNext.UseVisualStyleBackColor = true;
            this.buttonNext.Click += new System.EventHandler(this.buttonNext_Click);
            // 
            // buttonPrev
            // 
            this.buttonPrev.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.buttonPrev.Location = new System.Drawing.Point(117, 6);
            this.buttonPrev.Name = "buttonPrev";
            this.buttonPrev.Size = new System.Drawing.Size(39, 28);
            this.buttonPrev.TabIndex = 2;
            this.buttonPrev.Text = "＜";
            this.buttonPrev.UseVisualStyleBackColor = true;
            this.buttonPrev.Click += new System.EventHandler(this.buttonPrev_Click);
            // 
            // textBoxBillMonth
            // 
            this.textBoxBillMonth.Font = new System.Drawing.Font("ＭＳ ゴシック", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.textBoxBillMonth.Location = new System.Drawing.Point(56, 6);
            this.textBoxBillMonth.MaxLength = 4;
            this.textBoxBillMonth.Name = "textBoxBillMonth";
            this.textBoxBillMonth.Size = new System.Drawing.Size(53, 28);
            this.textBoxBillMonth.TabIndex = 1;
            // 
            // labelCount
            // 
            this.labelCount.AutoSize = true;
            this.labelCount.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.labelCount.Location = new System.Drawing.Point(195, 14);
            this.labelCount.Name = "labelCount";
            this.labelCount.Size = new System.Drawing.Size(42, 13);
            this.labelCount.TabIndex = 0;
            this.labelCount.Text = "0 / 0";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(3, 14);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(53, 12);
            this.label1.TabIndex = 0;
            this.label1.Text = "審査年月";
            // 
            // printDialog1
            // 
            this.printDialog1.Document = this.printDocument1;
            this.printDialog1.UseEXDialog = true;
            // 
            // printDocument1
            // 
            this.printDocument1.PrintPage += new System.Drawing.Printing.PrintPageEventHandler(this.printDocument1_PrintPage);
            // 
            // userControlImage1
            // 
            this.userControlImage1.AutoScroll = true;
            this.userControlImage1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.userControlImage1.Image = null;
            this.userControlImage1.ImageLocation = null;
            this.userControlImage1.Location = new System.Drawing.Point(0, 37);
            this.userControlImage1.Name = "userControlImage1";
            this.userControlImage1.Size = new System.Drawing.Size(562, 238);
            this.userControlImage1.TabIndex = 1;
            // 
            // UserControlTenken
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Controls.Add(this.userControlImage1);
            this.Controls.Add(this.panel1);
            this.Name = "UserControlTenken";
            this.Size = new System.Drawing.Size(562, 275);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.Panel panel1;
    private System.Windows.Forms.Label label1;
    private System.Windows.Forms.Label labelCount;
    private System.Windows.Forms.Button buttonPrev;
    private System.Windows.Forms.Button buttonNext;
    private UserControlImage userControlImage1;
    private System.Windows.Forms.PrintDialog printDialog1;
    private System.Drawing.Printing.PrintDocument printDocument1;
    private System.Windows.Forms.Button button1;
    private System.Windows.Forms.CheckBox checkBoxStamp;
    public System.Windows.Forms.TextBox textBoxBillMonth;
    public System.Windows.Forms.ComboBox comboBox;
}
