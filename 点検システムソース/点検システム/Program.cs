﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace 柔整点検システム
{
    static class Program
    {
        /// <summary>
        /// アプリケーションのメイン エントリ ポイントです。
        /// </summary>
        [STAThread]
        static void Main()
        {
            C_GlobalData.OpenDB();
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new FormTenken());
            C_GlobalData.CloseDB();
        }
    }
}
