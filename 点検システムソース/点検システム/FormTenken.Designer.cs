﻿namespace 柔整点検システム
{
    partial class FormTenken
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormTenken));
            this.panel1 = new System.Windows.Forms.Panel();
            this.lblver = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.textBoxHiho = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.userControlTenken1 = new UserControlTenken();
            this.userControlTenken2 = new UserControlTenken();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.lblver);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.textBoxHiho);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel1.Location = new System.Drawing.Point(897, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(130, 332);
            this.panel1.TabIndex = 0;
            // 
            // lblver
            // 
            this.lblver.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.lblver.AutoSize = true;
            this.lblver.Location = new System.Drawing.Point(24, 308);
            this.lblver.Name = "lblver";
            this.lblver.Size = new System.Drawing.Size(103, 15);
            this.lblver.TabIndex = 3;
            this.lblver.Text = "ver.20200213";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 198);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(55, 15);
            this.label2.TabIndex = 2;
            this.label2.Text = "label2";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 124);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(55, 15);
            this.label1.TabIndex = 1;
            this.label1.Text = "label1";
            // 
            // textBoxHiho
            // 
            this.textBoxHiho.Font = new System.Drawing.Font("ＭＳ ゴシック", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.textBoxHiho.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.textBoxHiho.Location = new System.Drawing.Point(6, 32);
            this.textBoxHiho.MaxLength = 8;
            this.textBoxHiho.Name = "textBoxHiho";
            this.textBoxHiho.Size = new System.Drawing.Size(99, 31);
            this.textBoxHiho.TabIndex = 0;
            this.textBoxHiho.TextChanged += new System.EventHandler(this.textBoxMonthInsureno_TextChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(4, 17);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(87, 15);
            this.label3.TabIndex = 0;
            this.label3.Text = "被保番８桁";
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.userControlTenken1);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.userControlTenken2);
            this.splitContainer1.Size = new System.Drawing.Size(897, 332);
            this.splitContainer1.SplitterDistance = 442;
            this.splitContainer1.TabIndex = 1;
            // 
            // userControlTenken1
            // 
            this.userControlTenken1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.userControlTenken1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.userControlTenken1.ImportType = 0;
            this.userControlTenken1.Location = new System.Drawing.Point(0, 0);
            this.userControlTenken1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.userControlTenken1.Name = "userControlTenken1";
            this.userControlTenken1.PreviewTextControl = this.label1;
            this.userControlTenken1.Size = new System.Drawing.Size(442, 332);
            this.userControlTenken1.TabIndex = 0;
            // 
            // userControlTenken2
            // 
            this.userControlTenken2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.userControlTenken2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.userControlTenken2.ImportType = 2;
            this.userControlTenken2.Location = new System.Drawing.Point(0, 0);
            this.userControlTenken2.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.userControlTenken2.Name = "userControlTenken2";
            this.userControlTenken2.PreviewTextControl = this.label2;
            this.userControlTenken2.Size = new System.Drawing.Size(451, 332);
            this.userControlTenken2.TabIndex = 0;
            // 
            // FormTenken
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1027, 332);
            this.Controls.Add(this.splitContainer1);
            this.Controls.Add(this.panel1);
            this.Font = new System.Drawing.Font("ＭＳ ゴシック", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FormTenken";
            this.Text = "申請書点検";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormTenken_FormClosing);
            this.Load += new System.EventHandler(this.FormTenken_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TextBox textBoxHiho;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private UserControlTenken userControlTenken1;
        private UserControlTenken userControlTenken2;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblver;
    }
}