﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace 柔整点検システム
{
    public partial class FormTenken : Form
    {
        /// <summary> 設定を読み込み、ダイアログに反映させる。</summary>
        void ReadSetting()
        {
            try
            {
                if (System.IO.File.Exists("fromsetting.ini"))
                {
                    int n;
                    string[] lines = System.IO.File.ReadAllLines("fromsetting.ini");
                    if (int.TryParse(lines[0], out n) && n > 0) textBoxHiho.Text = n.ToString("00000000");    // [0] 被保険者番号８桁
                    if (int.TryParse(lines[1], out n) && n >= 0) userControlTenken1.ImportType = n;           // [1] 確定済みのみのチェックボックスの設定(先々月)
                    if (int.TryParse(lines[2], out n) && n >= 0) userControlTenken2.ImportType = n;           // [2] 確定済みのみのチェックボックスの設定(先月)
                    return;
                }
            }
            catch (Exception)
            {
            }

            //// 設定読み込みに失敗 OR 設定ファイルなし            
            //userControlTenken1.ImportType = 0;   // 前々月 → 0:確定済みのみ
            //userControlTenken2.ImportType = 2;   // 前月   → 2:全画像
        }

        /// <summary> ダイアログの状態を設定に書き込む。</summary>
        void SaveSettings()
        {
            try
            {
                string[] lines = new []
                {
                    textBoxHiho.Text.Length == 8 ? textBoxHiho.Text : "",   // [0] 被保険者番号８桁
                    userControlTenken1.ImportType.ToString(),               // [1] 確定済みのみのチェックボックスの設定(先々月)
                    userControlTenken2.ImportType.ToString(),               // [2] 確定済みのみのチェックボックスの設定(先月)
                };
                System.IO.File.WriteAllLines("fromsetting.ini", lines);
            }
            catch (Exception)
            {
            }

        }


        public FormTenken()
        {
            InitializeComponent();

            label1.Text = label2.Text = "";

            userControlTenken1.textBoxBillMonth.TextChanged += textBoxMonthInsureno_TextChanged;
            userControlTenken2.textBoxBillMonth.TextChanged += textBoxMonthInsureno_TextChanged;
            userControlTenken1.comboBox.SelectedIndexChanged += textBoxMonthInsureno_TextChanged;
            userControlTenken2.comboBox.SelectedIndexChanged += textBoxMonthInsureno_TextChanged;
        }


        #region 20190505161027 furukawa 西暦から和暦変換

        /// <summary>
        /// 西暦から和暦変換
        /// </summary>
        /// <param name="tmpdt">西暦</param>
        /// <returns></returns>
        private string cngAdYM2GGYM(DateTime tmpdt)
        {
            string waYM = string.Empty;

            if (tmpdt.Year == 2019)
            {
                if (tmpdt.Month >= 5)
                {//令和
                    waYM = (tmpdt.Year - 2018).ToString("00") + tmpdt.Month.ToString("00");
                    return waYM;
                }
                else
                {//平成
                    waYM = (tmpdt.Year - 1988).ToString("00") + tmpdt.Month.ToString("00");
                    return waYM;

                }
            }
            else if (tmpdt.Year >= 2020)
            {
                //令和
                waYM = (tmpdt.Year - 2018).ToString("00") + tmpdt.Month.ToString("00");
                return waYM;
            }
            else
            {//平成
                waYM = (tmpdt.Year - 1988).ToString("00") + tmpdt.Month.ToString("00");
                return waYM;
            }
        }
        #endregion

        private void FormTenken_Load(object sender, EventArgs e)
        {



            // 前々月の表示
            DateTime d = DateTime.Now.AddMonths(-2);

            //20190505161115 furukawa st ////////////////////////
            //令和対応
            userControlTenken1.textBoxBillMonth.Text = cngAdYM2GGYM(d);
            //userControlTenken1.textBoxBillMonth.Text = (d.Year - 1988).ToString("00") + d.Month.ToString("00");
            //20190505161115 furukawa ed ////////////////////////


            // 前月の表示
            d = d.AddMonths(1);

            //20190505161230 furukawa st ////////////////////////
            //令和対応

            userControlTenken2.textBoxBillMonth.Text = cngAdYM2GGYM(d);
            //userControlTenken2.textBoxBillMonth.Text = (d.Year - 1988).ToString("00") + d.Month.ToString("00");
            //20190505161230 furukawa ed ////////////////////////




            // 設定を読み込み、ダイアログに反映させる。
            ReadSetting();
            //userControlTenken1.ImportType = 0;   // 前々月 → 0:確定済みのみ
            //userControlTenken2.ImportType = 2;   // 前月   → 2:全画像
            
            // 画像のクリア
            userControlTenken1.SetPreviewList(null);
            userControlTenken2.SetPreviewList(null);
        }



        long previewdMonth1 = 0;
        long previewdMonth2 = 0;
        int previewdImporttype1 = -1;
        int previewdImporttype2 = -1;

        string previewdInsureno = "";


        /// <summary> 年月・被保険者番号変更時の処理 </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void textBoxMonthInsureno_TextChanged(object sender, EventArgs e)
        {
            if (textBoxHiho.Text.Trim().Length != textBoxHiho.MaxLength) return;

            string strMonth;                                  // 審査月文字列
            int month;                                  // 審査月

            //bool kaku1 = userControlTenken1.checkBoxKakuteionly.Checked;    // 確定済 １
            //bool kaku2 = userControlTenken2.checkBoxKakuteionly.Checked;    // 確定済 ２

            
            if (int.TryParse(userControlTenken1.textBoxBillMonth.Text, out month) == true &&
                userControlTenken1.textBoxBillMonth.Text.Trim().Length == userControlTenken1.textBoxBillMonth.MaxLength &&
                (previewdMonth1 != month || previewdInsureno != textBoxHiho.Text.Trim() ||
                userControlTenken1.ImportType != previewdImporttype1)) 
            {

                strMonth = month.ToString("00");
                var list = GetPreviewData(textBoxHiho.Text, month, userControlTenken1.ImportType);

                userControlTenken1.SetPreviewList(list);
                previewdMonth1 = month;
                previewdImporttype1 = userControlTenken1.ImportType;
            }

            if (int.TryParse(userControlTenken2.textBoxBillMonth.Text, out month) == true &&
                userControlTenken2.textBoxBillMonth.Text.Trim().Length == userControlTenken2.textBoxBillMonth.MaxLength &&
                (previewdMonth2 != month || previewdInsureno != textBoxHiho.Text.Trim() || userControlTenken2.ImportType != previewdImporttype2))
            {

                strMonth = month.ToString("00");                
                var list = GetPreviewData(textBoxHiho.Text, month, userControlTenken2.ImportType);

                userControlTenken2.SetPreviewList(list);
                previewdMonth2 = month;
                previewdImporttype2 = userControlTenken2.ImportType;
            }

            previewdInsureno = textBoxHiho.Text.Trim();

        }
        

        /// <summary> プレビュー画像一覧を取得する </summary>
        /// <param name="insuredno">被保険者番号</param>
        /// <param name="month">審査月（平成年２桁＋月２桁）</param>
        /// <param name="kakutei">確定済（０：確定済み、１：国保画像、２：全画像）</param>
        /// <returns>プレビュー画像一覧</returns>
        
        List<UserControlTenken.PreviewData> GetPreviewData(string insuredno, int month, int kakutei)
        {
            try
            {
                //20190505163843 furukawa st ////////////////////////
                //入力チェックを１からにした

                // 入力チェック（審査月）
                if (month <1 || month > 9912) return null;              // R1年～H99年に限定する
                //if (month < 2001 || month > 9912) return null;              // H20年～H99年に限定する
                //20190505163843 furukawa ed ////////////////////////

                if (month % 100 < 1 || month % 100 > 13) return null;       // 1～12月に限定する


                // 入力チェック (電算管理番号)
                long l;
                if (long.TryParse(insuredno, out l) == false) return null;
                else if (l <= 0) return null;

                var outData = new List<UserControlTenken.PreviewData>();          // 出力データ
                var readDenList = new HashSet<string>();                          // 電算管理番号一覧 (重複防止)

                if (kakutei == 0)
                {
                    // 0 確定済みのみ
                    GetPreviewData_Scan(insuredno, month, true, outData, readDenList);      // スキャン画像から、確定済み画像を取得
                    GetPreviewData_Import(insuredno, month, true, outData, readDenList);    // 国保画像から、確定済み画像を取得
                }
                else if (kakutei == 1)
                {
                    // 1 国保画像みのみ
                    GetPreviewData_Import(insuredno, month, false, outData, readDenList);    // 国保画像から、確定済み or 未確定画像を取得
                }
                else
                {
                    // 2 全画像
                    GetPreviewData_Scan(insuredno, month, false, outData, readDenList);      // スキャン画像から、確定済み or 未確定画像画像を取得
                    GetPreviewData_Import(insuredno, month, false, outData, readDenList);    // 国保画像から、確定済み or 未確定画像を取得
                }


                if (outData == null) return null;
                else if (outData.Count <= 0) return null;
                else return outData;
            }
            catch (Exception ex)
            {
                C_GlobalData.OutputErrorLog(ex, true);
                return null;
            }

        }

        /// <summary> スキャン画像の検索 </summary>
        /// <param name="insuredno">被保険者番号</param>
        /// <param name="month">審査月</param>
        /// <param name="kakutei">確定済（チェックの有無）</param>
        /// <param name="outData">検索結果一覧(in/out)</param>
        /// <param name="readDenList">取り込み済み電算番号一覧(in/out)</param>

        void GetPreviewData_Scan(string insuredno, int month, bool kakutei,
            List<UserControlTenken.PreviewData> outData, HashSet<string> readDenList)

        {
            // 審査月・被保険者番号に該当するスキャンレセプトID一覧を取得する。
            // キー：レセプトID、 データ：テキストに出力する内容
            var readRidList = new Dictionary<long, string>();

            // ・ 確定済＝true  → 確定済みcvsデータ(tdata)から被保番を探し、rid(スキャン番号)から審査月を探す
            // ・ 確定済＝false → スキャンへの入力から被保番を探し、rid(レセプトのスキャン番号)から審査月を探す
            string query = kakutei ? "SELECT rid, outdata FROM receipts inner join tdata on receipts.outdata = tdata.id " +
                " WHERE rid >= :min AND rid <= :max AND tdata.insuredno = :insuredno order by rid" 
                : "SELECT rid, outdata FROM receipts WHERE rid >= :min AND rid <= :max AND insuredno = :insuredno order by rid";
            using (var cmd = new Npgsql.NpgsqlCommand(query, C_GlobalData.DBConn))
            {
                var sw = C_GlobalData.StopwatchBegin();

                cmd.Parameters.Add("min", NpgsqlTypes.NpgsqlDbType.Bigint);
                cmd.Parameters.Add("max", NpgsqlTypes.NpgsqlDbType.Bigint);
                cmd.Parameters.Add("insuredno", NpgsqlTypes.NpgsqlDbType.Text);
                cmd.Parameters["min"].Value = month * 1000000L;                             // スキャン画像ID MIN：2608 → 2608000000                
                cmd.Parameters["max"].Value = month * 1000000L + 999999L;                   // スキャン画像ID MAX：2608 → 2608999999
                cmd.Parameters["insuredno"].Value = insuredno;

                using (var r = cmd.ExecuteReader())
                {
                    while (r.Read())
                    {
                        string text = month.ToString("00年00月") + "審査" +                 // 審査月
                                      "\r\nｽｷｬﾝ." + r["rid"] +                              // スキャン画像のID
                                      "\r\nNo：" + r["outdata"] +                           // 電算番号
                                      (kakutei ? "\r\n確定済" : "");                        // 確定済み
                        readRidList.Add(Convert.ToInt64(r["rid"]), text);                   // スキャン画像ID一覧を保存する。 (キー：画像ID,データ：表示テキスト)
                        readDenList.Add(r["outdata"].ToString());                           // 画像に電算番号が付いている場合は、取り込み済み一覧に保存しておく（重複防止の為）
                    }
                }
                C_GlobalData.StopwatchEnd(sw, "スキャン画像一覧の取得", cmd);
            }


            // 続紙一覧を取得し、出力データに格納する。

            //20190813134456 furukawa st ////////////////////////
            
            
            //続紙を探す条件が大雑把で、全年月探していたので画像が出るのが遅かったので rid+100で区切った

            query = "SELECT * FROM receipts WHERE rid > :rid and rid <= :rid +100 ORDER BY rid";

                //query = "SELECT * FROM receipts WHERE rid > :rid ORDER BY rid";

            //20190813134456 furukawa ed ////////////////////////


            using (var cmd = new Npgsql.NpgsqlCommand(query, C_GlobalData.DBConn))
            {
                var sw = C_GlobalData.StopwatchBegin();

                cmd.Parameters.Add("rid", NpgsqlTypes.NpgsqlDbType.Bigint);
                foreach (var rid in readRidList)
                {
                    // 続紙よりも先に、原紙を出力する！
                    var dat = new UserControlTenken.PreviewData() { img = ScanImgPath(rid.Key), text = rid.Value };
                    outData.Add(dat);

            
                    // レセプト番号が、原紙より後のものを順番に探す！
                    cmd.Parameters["rid"].Value = rid.Key;
                    using (var r = cmd.ExecuteReader())
                    {
                        while (r.Read())
                        {
                            long id = Convert.ToInt64(r["rid"]);

                            
                            if (r["medimonth"].ToString() == "99")  // 審査月＝99 (続紙)
                            {                                
                                dat = new UserControlTenken.PreviewData()
                                {
                                    img = ScanImgPath(id),
                                    text = month.ToString("00年00月") + "審査" +                    // 審査月
                                           "\r\nｽｷｬﾝ." + r["rid"] +                                 // スキャン画像のID
                                           (kakutei ? "\r\nNo：" + r["outdata"] : "\r\n★続紙★")   // 電算番号-続紙番号（確定済みのみ）                                
                                };
                                outData.Add(dat);
                            }
                            else if (r["medimonth"].ToString() != "9999")
                            {
                                break;
                            }
                        }
                    }
                    
                }
                C_GlobalData.StopwatchEnd(sw, "続紙一覧の取得、枚数：" + readRidList.Count, cmd);
            }
        }



        /// <summary> 国保連取り込み画像の検索 </summary>
        /// <param name="insuredno">被保険者番号</param>
        /// <param name="month">審査月</param>
        /// <param name="kakutei">確定済（チェックの有無）</param>
        /// <param name="outData">検索結果一覧(in/out)</param>
        /// <param name="readDenList">取り込み済み電算番号一覧(in/out)</param>
        void GetPreviewData_Import(string insuredno, int month, bool kakutei,
            List<UserControlTenken.PreviewData> outData, HashSet<string> readDenList)
        {            

            // 先に、確定済画像を検索する
            string　query = kakutei ? "SELECT id, gid, fname FROM importreceipts WHERE id IN (SELECT id FROM tdata WHERE insuredno = :insuredno) AND billmonth = :billmonth" // 確定済画像一覧
                                    : "SELECT id, gid, fname FROM importreceipts WHERE insuredno = :insuredno AND billmonth = :billmonth";                                   // 確定前画像一覧

            // 審査月・被保険者番号に該当するスキャンレセプトID一覧を取得する。
            using (var cmd = new Npgsql.NpgsqlCommand(query, C_GlobalData.DBConn))
            {
                var sw = C_GlobalData.StopwatchBegin();
                cmd.Parameters.AddWithValue("insuredno", insuredno);
                cmd.Parameters.AddWithValue("billmonth", month);

                using (var r = cmd.ExecuteReader())
                {
                    while (r.Read())
                    {
                        string id = r["id"].ToString();
                        if (readDenList.Contains(id) == false)          // 電算管理番号の重複チェック
                        {
                            readDenList.Add(id);
                            outData.Add(new UserControlTenken.PreviewData()
                            {
                                img = ImportImgPath(Convert.ToInt32(r["gid"]), r["fname"].ToString()),
                                text = month.ToString("00年00月") + "審査\r\nNo." + id + "\r\n取込" + (kakutei ? "\r\n確定済" : "")
                            });
                        }
                    }
                }
                C_GlobalData.StopwatchEnd(sw, "国保連CSV画像一覧の取得", cmd);
            }
        }


        /// <summary>国保連画像のファイル名の取得 </summary>
        /// <param name="gid">国保連画像の取込グループID</param>
        /// <param name="fname">ファイル名（電算番号＋拡張子）</param>
        /// <returns>画像ファイル名（フルパス）</returns>
        string ImportImgPath(int gid, string fname)
        {
            return C_GlobalData.ReceimportImgRoot + "\\" + (gid / 1000).ToString("000") + "000\\" + (gid).ToString("000000") + "\\" + fname;
        }


        /// <summary>スキャm画像のファイル名の取得 </summary>
        /// <param name="rid">レセプトID</param>
        /// <returns>画像ファイル名（フルパス）</returns>
        string ScanImgPath(long rid)
        {
            return C_GlobalData.RecescanImgRoot + "\\" + (rid / 10000).ToString() + "\\" + rid.ToString() + ".gif";
        }

        private void FormTenken_FormClosing(object sender, FormClosingEventArgs e)
        {
            SaveSettings();
        }

    }
}
