﻿using System;
using System.Data;
using System.Globalization;
using System.IO;
using System.Collections.Generic;
using System.Collections;
using System.Threading;
using System.Data.Common;
using System.Text;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using Npgsql;


/// <summary> グローバルなデータ・処理を格納するクラス </summary>
public class C_GlobalData
{
    /// <summary>DBをOPENする。</summary>
    public static void OpenDB()
    {
        try
        {
            // iniファイル（tenkenDB.ini）の読み込み
            string[] iniData = File.ReadAllLines(Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location)
                                                 + @"\tenkenDB.ini"
                                                 , Encoding.Default);            
            // １行目：scanフォルダ
            RecescanImgRoot = iniData[0];
            // ２行目：国保連画像フォルダ
            ReceimportImgRoot = iniData[1];
            // ３行目以降：SQL接続設定
            string  conn = ""; for (int i = 2; i < iniData.Length; i++) conn += iniData[i] + "\r\n";
            DBConn = new Npgsql.NpgsqlConnection(conn);
            DBConn.Open();
        }
        catch (Exception ex)
        {
            OutputErrorLog(ex, true);
            Application.Exit();
        }
    }

    /// <summary>DBをCloseする。</summary>
    public static void CloseDB()
    {
        try
        {
            // recescan (従来システム・紙データをスキャン) のDBをClose
            DBConn.Close();
            DBConn.Dispose();
            DBConn = null;
        }
        catch (Exception ex)
        {
            OutputErrorLog(ex, true);
            Application.Exit();   
        }
    }

    /// <summary>recescan (従来システム・紙データをスキャン) の 画像ルートフォルダ </summary>
    public static string RecescanImgRoot { get; set; }

    /// <summary>receimport(国保連画像取り込み) の 画像ルートフォルダ </summary>
    public static string ReceimportImgRoot { get; set; }

    /// <summary>PostgreSQLコネクション </summary>
    public static Npgsql.NpgsqlConnection DBConn { get; set; }

        /// <summary> エラーログの出力 </summary>
    /// <param name="ex">エラー</param>
    /// <param name="megBox">メッセージ表示の有無</param>
    public static void OutputErrorLog(Exception ex, bool megBox)
    {   
        DateTime now = DateTime.Now;
        if (megBox) MessageBox.Show(now.ToString() + "\r\n" + ex.Message, "エラー", MessageBoxButtons.OK, MessageBoxIcon.Error);
                
        try
        {
            string dir = Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location) + @"\Log\" + now.ToString("yyyyMM");
            string fn = dir + "\\Error-" + now.ToString("yyyyMMdd") + ".txt";

            if (Directory.Exists(dir) == false) Directory.CreateDirectory(dir);
            File.AppendAllText(fn, now.ToString() + "\r\n" + ex.ToString() + "\r\n\r\n");
        }
        catch (Exception)
        {
        }
    }
    
    /// <summary> タイマー計測を開始する </summary>
    /// <returns>ストップウオッチオブジェクト</returns>
    public static System.Diagnostics.Stopwatch StopwatchBegin()
    {
        var sw = new System.Diagnostics.Stopwatch();
        sw.Start();
        return sw;
    }


    /// <summary> タイマー計測を終了する。遅い時はログに出力する。 </summary>
    /// <param name="sw">ストップウオッチオブジェクト</param>
    /// <param name="logText">追加で出力するテキスト</param>
    public static void StopwatchEnd(System.Diagnostics.Stopwatch sw, string logText, Npgsql.NpgsqlCommand cmd = null)
    {
        sw.Stop();

        // ログ出力の閾値 （＝SlowLog.* の拡張子・単位ミリ秒、 ファイルなしの時は1000ミリ秒）の取得
        long min, millsec = sw.ElapsedMilliseconds;
        try
        {
            var list = Directory.GetFiles(Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location), "SlowLog.*", SearchOption.TopDirectoryOnly);
            if (list == null)
            {
                min = 1000;
            }
            else if (list.Length <= 0)
            {
                min = 1000;
            }
            else
            {
                string ext = Path.GetExtension(list[0]).Replace(".", "");    // SlowLog.*** の拡張子の取得
                if (long.TryParse(ext, out min) == false) min = 1000;
            }
        }
        catch (Exception)
        {
            min = 1000;
        }

        // 閾値以上 → ログ出力
        if (millsec >= min)
        {
            try
            {
                DateTime now = DateTime.Now;


                string dir = Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location) + @"\Log\" + now.ToString("yyyyMM");
                string fn = dir + "\\Slow-" + now.ToString("yyyyMMdd") + ".txt";

                if (Directory.Exists(dir) == false) Directory.CreateDirectory(dir);

                string text = now.ToString() + " " + millsec.ToString() + "ミリ秒 " + logText;
                if (cmd != null)
                {
                    string query = cmd.CommandText;
                    text += "\r\n" + query;
                    foreach (Npgsql.NpgsqlParameter p in cmd.Parameters) query = query.Replace(":" + p.ParameterName, (p.Value == null ? "NULL" : "'" + p.Value.ToString() + "'"));
                    text += "\r\n" + query;
                }
                text += "\r\n" + Environment.StackTrace + "\r\n\r\n";
                File.AppendAllText(fn, text);
            }

            //20190514115429 furukawa st ////////////////////////
            //エラーメッセージ出す

            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            //catch (Exception)
            //{
            //}
            //20190514115429 furukawa ed ////////////////////////
        }

    }
}
