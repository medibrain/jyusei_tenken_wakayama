﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

public partial class UserControlImage : UserControl
{
    public UserControlImage()
    {
        InitializeComponent();
    }

    /// <summary>
    /// System.Windows.Forms.PictureBox によって表示されるイメージを取得します。
    /// </summary>
    public Image Image
    {
        get { return pictureBox.Image; }
        set { pictureBox.Image = value; }
    }

    /// <summary> System.Windows.Forms.PictureBox に表示するイメージのパスまはた URL を取得または設定します。</summary>
    public string ImageLocation
    {
        get
        {
            return pictureBox.ImageLocation;
        }
        set
        {
            try
            {
                // 元画像の削除
                if (pictureBox.Image != null) pictureBox.Image.Dispose();
            }
            catch (Exception)
            {
            }
            pictureBox.ImageLocation = "";
            pictureBox.ImageLocation = value;
        }
    }

    /// <summary> System.Windows.Forms.PictureBox に表示するイメージの再描画</summary>
    public void UpdateView()
    {
        var loc = pictureBox.ImageLocation;
        try
        {
            // 元画像の削除
            if (pictureBox.Image != null) pictureBox.Image.Dispose();
        }
        catch (Exception)
        {
        }
        pictureBox.ImageLocation = "";
        pictureBox.ImageLocation = loc;
    }

    /// <summary>表示している画像を９０度回転させる（画像ファイルも回転）</summary>
    /// <param name="clockwise">trueなら時計まわり</param>
    /// <param name="views">対象となるビュー（画像表示コントロール）一覧</param>
    static public void ImageRotate(bool clockwise, params UserControlImage[] views)
    {
        try
        {
            using (var bmp = Image.FromFile(views[0].ImageLocation))
            {
                bmp.RotateFlip(clockwise ? RotateFlipType.Rotate90FlipNone : RotateFlipType.Rotate270FlipNone);
                bmp.Save(views[0].ImageLocation, bmp.RawFormat);
            }
            foreach (var item in views) item.UpdateView();
        }
        catch (Exception ex)
        {
        }

        
    }

    /// <summary>表示している画像を９０度回転させる（画像ファイルも回転）</summary>
    /// <param name="clockwise">trueなら時計まわり</param>
    public void ImageRotate(bool clockwise)
    {
        try
        {
            using (var bmp = Image.FromFile(ImageLocation))
            {
                bmp.RotateFlip(clockwise ? RotateFlipType.Rotate90FlipNone : RotateFlipType.Rotate270FlipNone);
                bmp.Save(ImageLocation, bmp.RawFormat);
                UpdateView();
            }
        }
        catch (Exception ex)
        {
        }
    }

    public void InitPictureBoxLocation()
    {
        this.AutoScrollPosition = new Point(0, 0);
        pictureBox.Size = this.Size;
        pictureBox.Dock = DockStyle.Fill;
    }

    /// <summary> 画像のサイズ・スクロール位置の更新 </summary>
    /// <param name="pictureSize">画像のサイズ</param>
    /// <param name="sclool">スクロール位置</param>
    public void SetPictureBoxLocation(Size pictureSize, Point sclool)
    {
        if (pictureSize.Height <= this.Height && pictureSize.Width <= this.Width)
        {
            // 画像のサイズ ＜ 枠のサイズの時 → 完全に枠の大きさに合わせる！
            this.AutoScrollPosition = new Point(0, 0);
            pictureBox.Size = this.Size;
            pictureBox.Dock = DockStyle.Fill;
        }
        else
        {
            pictureBox.Dock = DockStyle.None;
            pictureBox.Size = pictureSize;
            this.AutoScrollPosition = sclool;
        }
    }


    public void GetPictureBoxLocation(out Size pictureSize, out Point sclool)
    {
        if (pictureBox.Dock == DockStyle.Fill)
        {
            pictureSize = this.Size;
            sclool = new Point(0, 0);
        }
        else
        {
            pictureSize = pictureBox.Size;
            sclool = new Point(-this.AutoScrollPosition.X, -this.AutoScrollPosition.Y);
        }
    }


    private void pictureBox_MouseClick(object sender, MouseEventArgs e)
    {
        if (e.Button == System.Windows.Forms.MouseButtons.Left || e.Button == System.Windows.Forms.MouseButtons.Right)
        {
            // 画像サイズ ・ スクロール位置 の取得
            Size pictureSize;       // 画像サイズ
            Point sclool;           // スクロール位置
            GetPictureBoxLocation(out pictureSize, out sclool);
            
            // 拡大率 （左クリック 1.3倍ズーム、 右 1/1.3 縮小）
            double rate = (e.Button == System.Windows.Forms.MouseButtons.Left ? 1.1 : 1 / 1.1);


            // 画像サイズ ・ スクロール位置 の更新
            pictureSize.Width = (int)(rate * pictureSize.Width);
            pictureSize.Height = (int)(rate * pictureSize.Height);
            sclool.X = (int)(rate * sclool.X);
            sclool.Y = (int)(rate * sclool.Y);
            SetPictureBoxLocation(pictureSize, sclool);
        }
    }
}
