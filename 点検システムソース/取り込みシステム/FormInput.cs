﻿#define NUM_CHECK           // 被保険者番号（数字入力）のチェック

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace 取り込みシステム
{
    public partial class FormInput : Form
    {
        int m_Gid;

        class C_InputData
        {
            /// <summary>電算管理番号</summary>
            public string id;
            /// <summary>fname</summary>
            public string fname;
            /// <summary>被保険者番号</summary>
            public string no;
            /// <summary>tdataから取得した被保険者番号</summary>
            public string td;
        };

        /// <summary>電算管理番号・入力被保険者番号のリスト</summary>
        List<C_InputData> m_IdList = new List<C_InputData>();
        
        /// <summary>入力データ一覧のインデックス</summary>
        int m_Index = 0;
                        
        public FormInput(int gid, int billmonth)
        {
            try
            {
                m_Gid = gid;

                InitializeComponent();
                labelGID.Text = "No." + gid + "\r\n" + billmonth.ToString("00年00月") + "審査";
                labelCaution.Text = "";

                // 電算管理番号一覧の取得
                string query = "SELECT importreceipts.id, importreceipts.insuredno, importreceipts.fname, tdata.insuredno FROM importreceipts LEFT JOIN tdata on importreceipts.id = tdata.id WHERE importreceipts.gid = :gid order by importreceipts.id";
                using (var cmd = new Npgsql.NpgsqlCommand(query, C_ImportGlobal.DBConn))
                {
                    cmd.Parameters.AddWithValue("gid", gid);
                    using (var r = cmd.ExecuteReader())
                    {
                        while (r.Read()) m_IdList.Add(new C_InputData() { id = r[0].ToString().Trim(), fname = r[2].ToString().Trim(), no = r[1].ToString().Trim(), td = r[3].ToString().Trim() });
                    }
                }



            }
            catch (Exception ex)
            {
                C_ImportGlobal.OutputErrorLog(ex, false);
            }

        }
        
        private void FormInput_Load(object sender, EventArgs e)
        {
            try
            {
                for (int i = 0; i < m_IdList.Count; i++)
                {
                    if (m_IdList[i].no == "")
                    {
                        m_Index = i;
                        break;
                    }
                }


                // データ表示の実行
                UpdateView();
            }
            catch (Exception ex)
            {
                // 異常時はクローズする！
                C_ImportGlobal.OutputErrorLog(ex, true);
                this.Close();
            }
            textBox1_TextChanged(sender, e);
        }


        /// <summary>表示の更新を行う</summary>
        void UpdateView()
        {
            if (m_IdList.Count <= 0) throw new Exception("エラー\r\nデータ無し");
                        
            // indexの調整
            if (m_Index < 0) m_Index = 0;
            else if (m_Index >= m_IdList.Count) m_Index = m_IdList.Count - 1;

            var dat = m_IdList[m_Index];
            labelCounter.Text = (m_Index + 1).ToString() + " / " + m_IdList.Count.ToString();       // カウント表示
            labelID.Text = "電算管理番号\r\n" + dat.id;                                             // 電算番号表示
            userControlImage1.ImageLocation = C_ImportGlobal.ReceImportImgPath(m_Gid, dat.fname);   // 画像表示
            if (dat.td != "") textBox1.Text = dat.td;                                               // 被保険者番号表示（tdataより取得）
            else textBox1.Text = dat.no;                                                            // 被保険者番号表示（前回の入力内容）

            textBox1.Select();
            textBox1.SelectAll();
        }



        enum E_Check { OK, Empty, KetaError,  NumError}

        /// <summary>被保険者番号の入力チェック</summary>
        /// <returns></returns>
        E_Check InputCheck()
        {
            // 未入力チェック
            if (textBox1.Text.Trim().Length == 0) return E_Check.Empty;

            // 桁数エラーチェック
            if (textBox1.Text.Trim().Length != textBox1.MaxLength) return E_Check.KetaError;

#if NUM_CHECK
            // 番号エラーチェック
            long n;
            if (long.TryParse(textBox1.Text, out n) == false || n <= 0) return E_Check.NumError;
#endif
            return E_Check.OK;
        }



        /// <summary>被保険者番号入力時の処理</summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            try
            {
                switch (InputCheck())
                {
                    case E_Check.OK:
                        // 被保険者番号の入力内容（桁数・フォーマットは正常） → 既存のデータと比較                        
                        using (var cmd = new Npgsql.NpgsqlCommand("SELECT * FROM tdata WHERE insuredno = :insuredno", C_ImportGlobal.DBConn))
                        {
                            cmd.Parameters.AddWithValue("insuredno", textBox1.Text);
                            using (var r = cmd.ExecuteReader())
                            {
                                if (r.Read())
                                {
                                    // tdata (国保連データ)上の被保険者番号と一致する
                                    labelCaution.Text = "";
                                    return;
                                }
                            }
                        }
                        using (var cmd = new Npgsql.NpgsqlCommand("SELECT * FROM importreceipts WHERE insuredno = :insuredno AND NOT(id = :id)", C_ImportGlobal.DBConn))
                        {
                            cmd.Parameters.AddWithValue("insuredno", textBox1.Text);
                            cmd.Parameters.AddWithValue("id", m_IdList[m_Index].id);
                            using (var r = cmd.ExecuteReader())
                            {
                                if (r.Read()) labelCaution.Text = "";
                                else labelCaution.Text = "新規の番号です。\r\n入力を確認して下さい。";
                            }
                        }
                        break;
                    case E_Check.Empty:
                    case E_Check.KetaError:
                        labelCaution.Text = "８桁入力して下さい。";
                        break;
                    case E_Check.NumError:
                        labelCaution.Text = "エラー\r\n番号を入力して下さい。";
                        break;
                    default:
                        break;
                }

            }
            catch (Exception ex)
            {
                labelCaution.Text = "入力を確認して下さい。";
                C_ImportGlobal.OutputErrorLog(ex, false);
            }

        }


        enum e_UpdateArg { first, prev, next, last, ok }

        void DBUpdate(e_UpdateArg arg)
        {
            try
            {
                var chk = InputCheck();                     // 被保険者番号の入力チェック (1:正常／-1:異常／0:未入力)
                var val = m_IdList[m_Index];


                if (chk == E_Check.KetaError || chk == E_Check.NumError)
                {
                    // 入力エラー
                    textBox1.Select();
                    textBox1.SelectAll();
                    return;
                }
                else if (chk == E_Check.Empty && arg == e_UpdateArg.ok)
                {
                    // 空白入力でOKボタン
                    textBox1.Select();
                    textBox1.SelectAll();
                    return;
                }
                
                // DBの更新
                if (chk == E_Check.OK && textBox1.Text.Trim() != val.no)
                {
                    using (var cmd = new Npgsql.NpgsqlCommand("UPDATE importreceipts SET insuredno = :insuredno, inputdate = :inputdate WHERE id = :id", C_ImportGlobal.DBConn))
                    {
                        cmd.Parameters.AddWithValue("id", val.id);
                        cmd.Parameters.AddWithValue("insuredno", textBox1.Text.Trim());
                        cmd.Parameters.AddWithValue("inputdate", DateTime.Now);
                        cmd.ExecuteNonQuery();

                        val.no = textBox1.Text.Trim();
                    }
                }


                // 次に移る
                int next;
                switch (arg)
                {
                    case e_UpdateArg.first:
                        next = 0;
                        break;
                    case e_UpdateArg.last:
                        next = m_IdList.Count - 1;
                        break;
                    case e_UpdateArg.prev:
                        next = m_Index - 1;
                        break;
                    case e_UpdateArg.next:
                    case e_UpdateArg.ok:
                    default:
                        next = m_Index + 1;
                        break;
                }



                if (next >= 0 && next < m_IdList.Count)
                {
                    m_Index = next;
                    UpdateView();
                }
                else if (arg == e_UpdateArg.ok)
                {
                    MessageBox.Show("入力完了");
                }
            }
            catch (Exception ex)
            {
                C_ImportGlobal.OutputErrorLog(ex, true);
            }
        }



        private void button_Click(object sender, EventArgs e)
        {
            try
            {
                var name = ((Button)sender).Name;                          
                if(name == button1st.Name) DBUpdate(e_UpdateArg.first);
                else if (name == buttonPrev.Name) DBUpdate(e_UpdateArg.prev);
                else if (name == buttonNext.Name) DBUpdate(e_UpdateArg.next);
                else if (name == buttonLast.Name) DBUpdate(e_UpdateArg.last);
                else if (name == buttonOK.Name) DBUpdate(e_UpdateArg.ok);
            }
            catch (Exception ex)
            {
                C_ImportGlobal.OutputErrorLog(ex, true);
            }

        }

        private void buttonDelete_Click(object sender, EventArgs e)
        {

            if (MessageBox.Show("削除しますか？", "削除確認", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) != DialogResult.Yes) return;

            C_InputData val ;
            try 
	        {
                val = m_IdList[m_Index];
	        }
	        catch (Exception ex)
	        {
                C_ImportGlobal.OutputErrorLog(ex, true);
                return;
	        }

            // 画像ファイルの削除
            try
            {
                userControlImage1.ImageLocation = "";
                System.IO.File.Delete(C_ImportGlobal.ReceImportImgPath(m_Gid, val.fname));
	        }
	        catch (Exception ex)
	        {
                C_ImportGlobal.OutputErrorLog(ex, false);
	        }

            // DBからの削除
            try
            {
                using (var cmd = new Npgsql.NpgsqlCommand("DELETE FROM  importreceipts WHERE id = :id", C_ImportGlobal.DBConn))
                {
                    cmd.Parameters.AddWithValue("id", val.id);
                    cmd.ExecuteNonQuery();
                }

                m_IdList.RemoveAt(m_Index);
                UpdateView();
            }
            catch (Exception ex)
            {
                C_ImportGlobal.OutputErrorLog(ex, true);
            }
        }

        private void textBox1_KeyDown(object sender, KeyEventArgs e)
        {
            //if (e.KeyCode == Keys.Enter && InputCheck() == E_Check.OK) buttonOK.Select();
            if (e.KeyCode == Keys.Enter) DBUpdate(e_UpdateArg.ok);
        }
    }
}
