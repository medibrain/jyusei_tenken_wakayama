﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace 取り込みシステム
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        
        private void Form1_Load(object sender, EventArgs e)
        {
            // 一覧表示の更新
            UpdateView();
        }

        /// <summary>レセプト取り込み（ファイル指定）</summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button1_Click(object sender, EventArgs e)
        {
            this.Visible = false;
            using (var dlg = new FormInport()) dlg.ShowDialog();
            this.Visible = true;
            UpdateView();
        }

        private void buttonSeek_Click(object sender, EventArgs e)
        {
            UpdateView();
        }

        /// <summary>グループ一覧を更新する</summary>
        void UpdateView()
        {
            try
            {                
                if (dataGridView1.Columns.Contains("入力") == false)
                {
                    //DataGridViewButtonColumnの作成
                    DataGridViewButtonColumn column = new DataGridViewButtonColumn();
                    //列の名前を設定
                    column.Name = "入力";
                    column.HeaderText = "";
                    //全てのボタンに"詳細閲覧"と表示する
                    column.UseColumnTextForButtonValue = true;
                    column.Text = "入力";
                    //DataGridViewに追加する
                    dataGridView1.Columns.Add(column);
                }


                int year, month = 0;
                if (int.TryParse(textBoxY2.Text, out year) == false) year = 0;
                if (int.TryParse(textBoxM2.Text, out month) == false) month = 0;

                var whereList = new List<string>();
                if (checkBox1.Checked) whereList.Add("complete = 0");
                if (year > 0 && month >= 1 && month <= 12) whereList.Add("billmonth = " + year.ToString("00") + month.ToString("00"));
                
                string query = "SELECT gid, scandate, billmonth, cntall, cntinput, begindate, endate FROM importgroups";
                if (whereList.Count > 0) query += " WHERE " + string.Join(" AND ", whereList);
                query += " ORDER BY gid";

                
                using (var dt = new DataTable())
                using (var da = new Npgsql.NpgsqlDataAdapter(query, C_ImportGlobal.DBConn))
                {
                    da.Fill(dt);
                    dataGridView1.DataSource = dt;

                    foreach (DataGridViewColumn item in dataGridView1.Columns)
                    {
                        item.ReadOnly = true;
                    }

                    dataGridView1.Columns["gid"].HeaderText = "No";
                    dataGridView1.Columns["scandate"].HeaderText = "取込日";
                    dataGridView1.Columns["billmonth"].HeaderText = "審査月";
                    dataGridView1.Columns["billmonth"].ReadOnly = false;
                    dataGridView1.Columns["cntall"].HeaderText = "全件数";
                    dataGridView1.Columns["cntall"].DefaultCellStyle.Format = "0 件";
                    dataGridView1.Columns["cntinput"].HeaderText = "入力済";
                    dataGridView1.Columns["cntinput"].DefaultCellStyle.Format = "0 件";

                    dataGridView1.Columns["begindate"].HeaderText = "入力開始";
                    dataGridView1.Columns["endate"].HeaderText = "入力完了";

                    dataGridView1.DefaultCellStyle.BackColor = Color.FromArgb(240, 240, 240);
                    dataGridView1.Columns["入力"].DefaultCellStyle.BackColor = 
                    dataGridView1.Columns["billmonth"].DefaultCellStyle.BackColor = Color.White;

                    dataGridView1.AutoResizeColumns(DataGridViewAutoSizeColumnsMode.DisplayedCells);
                }
            }
            catch (Exception ex)
            {
                C_ImportGlobal.OutputErrorLog(ex, false);
            }
        }

        private void dataGridView1_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            dataGridView1.CancelEdit();
        }

        private void dataGridView1_CellValidating(object sender, DataGridViewCellValidatingEventArgs e)
        {
            try
            {
                var gid = dataGridView1["gid", e.RowIndex].Value;
                if (dataGridView1.Columns[e.ColumnIndex].Name == "billmonth")
                {
                    int month;
                    if (int.TryParse(e.FormattedValue.ToString(), out month) == false || month < 2000 || (month % 100) < 1 || (month % 100) > 12)
                    {
                        dataGridView1.CancelEdit();
                        return;
                    }
                    else if(month == Convert.ToInt32(dataGridView1[e.ColumnIndex, e.RowIndex].Value))
                    {
                    }
                    else if (MessageBox.Show("審査月を" + month.ToString("00年00月") + "に変更しますか？", "変更確認", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                    {
                        using (var c = new Npgsql.NpgsqlCommand("UPDATE importgroups   SET billmonth = :billmonth WHERE gid = :gid;" +
                                                               "UPDATE importreceipts SET billmonth = :billmonth WHERE gid = :gid;",
                                                               C_ImportGlobal.DBConn))
                        {
                            c.Parameters.AddWithValue("gid", gid);
                            c.Parameters.AddWithValue("billmonth", month);
                            c.ExecuteNonQuery();
                        }
                    }
                    else
                    {
                        dataGridView1.CancelEdit();
                        return;
                    }
                }

            }
            catch (Exception ex)
            {
                C_ImportGlobal.OutputErrorLog(ex, true);
            }
        }


        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            //"Button"列ならば、ボタンがクリックされた
            if (dataGridView1.Columns[e.ColumnIndex].Name == "入力")
            {
                int gid = Convert.ToInt32(dataGridView1["gid", e.RowIndex].Value);
                int billmonth = Convert.ToInt32(dataGridView1["billmonth", e.RowIndex].Value);
                
                C_ImportGlobal.ImportTdata(gid);
                using (var dlg = new FormInput(gid, billmonth)) dlg.ShowDialog();
                C_ImportGlobal.UpdateGroupInfo(gid);
                UpdateView();
            }

        }


    }
}
