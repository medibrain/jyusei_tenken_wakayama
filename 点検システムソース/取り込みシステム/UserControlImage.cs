﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

public partial class UserControlImage : UserControl
{
    public UserControlImage()
    {
        InitializeComponent();
    }

    /// <summary>
    /// System.Windows.Forms.PictureBox によって表示されるイメージを取得または設定します。
    /// </summary>
    public Image Image
    {
        get { return pictureBox.Image; }
        set { pictureBox.Image = value; }
    }

    /// <summary>
    /// System.Windows.Forms.PictureBox に表示するイメージのパスまはた URL を取得または設定します。
    /// </summary>
    public string ImageLocation
    {
        get { return pictureBox.ImageLocation; }
        set { pictureBox.ImageLocation = value; }
    }

    /// <summary> 画像のサイズ（縮小表示）のモード一覧 </summary>
    enum PictureSizeMode
    {
        /// <summary> パネルに合わせる（スクロールしない） </summary>
        PanelSize,
        /// <summary> X・Yいずれかのみパネルに合わせる（一方はスクロールする） </summary>
        XorYSize,
        /// <summary> イメージの大きさに合わせる（X・Y両方向でスクロールする） </summary>
        ImageSize,
    } ;

    PictureSizeMode m_ePictureBoxSizeMode = PictureSizeMode.PanelSize;

    /// <summary>
    /// ピクチャーボックスのパネルサイズ変更時・イメージサイズ変更時に、高さ・幅を調整する。
    /// </summary>
    private void updatePictureBoxSize()
    {
        if (pictureBox.Image == null) return;

        // X・Yいずれかのみパネルに合わせる（一方はスクロールする）モードの時のみ、調整する
        if (m_ePictureBoxSizeMode == PictureSizeMode.XorYSize)
        {
            try
            {
                // 比率 (枠のサイズ / 画像のサイズ) を計算する
                double rate_X = (double)Width / (double)pictureBox.Image.Size.Width;
                double rate_Y = (double)Height / (double)pictureBox.Image.Size.Height;

                if (rate_X > rate_Y)
                {
                    // X方向の比率 > Y方向の比率の時               
                    pictureBox.Dock = DockStyle.Top;                                // ピクチャーの幅を枠に合わせる
                    pictureBox.Height = (int)(pictureBox.Image.Height * rate_X);    // ピクチャーの高さを、X方向の比率に合わせる
                }
                else
                {
                    //                        pictureBox.Height = panel_PictureBox.Height;
                    pictureBox.Dock = DockStyle.Left;                               // ピクチャーの幅を枠に合わせる
                    pictureBox.Width = (int)(pictureBox.Image.Width * rate_Y);      // ピクチャーの幅を、Y方向の比率に合わせる
                }
            }
            catch (Exception)
            {
            }
        }

    }

    /// <summary>
    /// ピクチャーボックスクリック時、スクロールのモードを変える
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void pictureBox_Click(object sender, EventArgs e)
    {
        // 先に状態遷移させる。
        switch (m_ePictureBoxSizeMode)
        {
            case PictureSizeMode.PanelSize:
                // パネルに合わせる（スクロールしない）→ X・Yいずれかのみパネルに合わせる（一方はスクロールする）
                m_ePictureBoxSizeMode = PictureSizeMode.XorYSize;
                break;
            case PictureSizeMode.XorYSize:
                // X・Yいずれかのみパネルに合わせる（一方はスクロールする）→ イメージの大きさに合わせる（X・Y両方向でスクロールする）
                m_ePictureBoxSizeMode = PictureSizeMode.ImageSize;
                break;
            case PictureSizeMode.ImageSize:
            default:
                // イメージの大きさに合わせる（X・Y両方向でスクロールする）→パネルに合わせる（スクロールしない） 
                m_ePictureBoxSizeMode = PictureSizeMode.PanelSize;
                break;
        }

        switch (m_ePictureBoxSizeMode)
        {
            case PictureSizeMode.PanelSize:
                // パネルに合わせる（スクロールしない）
                pictureBox.Dock = DockStyle.Fill;                   // ピクチャーボックスのサイズを、枠に合わせる
                pictureBox.SizeMode = PictureBoxSizeMode.Zoom;      // 画像の大きさを、自動縮小（ピクチャーボックスに合わせる）
                break;
            case PictureSizeMode.XorYSize:
                // X・Yいずれかのみパネルに合わせる（一方はスクロールする）
                pictureBox.SizeMode = PictureBoxSizeMode.Zoom;      // 画像の大きさを、自動縮小（ピクチャーボックスに合わせる）
                break;
            case PictureSizeMode.ImageSize:
                // イメージの大きさに合わせる（X・Y両方向でスクロールする）
                pictureBox.Dock = DockStyle.Fill;
                pictureBox.Dock = DockStyle.None;                   // ピクチャーボックスのサイズを、枠に合わせない
                pictureBox.SizeMode = PictureBoxSizeMode.AutoSize;  // ピクチャーボックスの大きさを、画像に合わせる
                break;
            default:
                break;
        }
        updatePictureBoxSize();
    }

    /// <summary>
    /// ピクチャーボックスのイメージ読み込み完了時、全体表示（スクロールなし）に切り替える
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void pictureBox_LoadCompleted(object sender, AsyncCompletedEventArgs e)
    {
        updatePictureBoxSize();
    }

    private void UserControlImage_SizeChanged(object sender, EventArgs e)
    {
        updatePictureBoxSize();
    }
}
