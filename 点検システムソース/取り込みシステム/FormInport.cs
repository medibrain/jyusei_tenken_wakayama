﻿// ファイル名（電算管理番号）の数字チェックの有無
#define CHK_NUM

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;


namespace 取り込みシステム
{
    public partial class FormInport : Form
    {
        ///// <summary>取込元フォルダ （C:\Import）</summary>
        //string ImportFromDir { get { return C_ImportGlobal.ReceimportImgRoot + @"\取込画像"; } }

        ///// <summary>取込元フォルダに表示するコメント（空のファイル）</summary>
        //string ImportFromDirComment { get { return ImportFromDir + @"\★ここに取込元の画像を入れて下さい★"; } }

        /// <summary>取込拡張子</summary>
        string[] ImportExt = {"gif", "jpeg", "jpg", "tif", "tiff", "png"};
        
        public FormInport()
        {
            InitializeComponent();
        }

        private void FormInport_Load(object sender, EventArgs e)
        {
            this.AllowDrop = true;

            // 前月の表示
            DateTime d = DateTime.Now.AddMonths(-1);

            //20190513102412 furukawa st ////////////////////////
            //前月令和対応

            if ((d.Year == 2019 && d.Month >= 5) || d.Year>=2020 )
            {
                textBoxY1.Text = (d.Year - 2018).ToString();
            }

            else { 

                textBoxY1.Text = (d.Year - 1988).ToString();
            }

            //DateTime d = DateTime.Now.AddMonths(-1);
            //textBoxY1.Text = (d.Year - 1988).ToString();
            //20190513102412 furukawa ed ////////////////////////

            textBoxM1.Text = d.Month.ToString();

            //// 取り込み元のフォルダの表示
            //label1.Text = "取込元\r\n" + ImportFromDir;
            //if (Directory.Exists(ImportFromDir) == false) Directory.CreateDirectory(ImportFromDir);
            //if (File.Exists(ImportFromDirComment) == false) File.WriteAllText(ImportFromDirComment, "");
            //System.Diagnostics.Process.Start(ImportFromDir);
        }

        bool m_cancelFlag;


        /// <summary> バックグラウンド処理（取り込み）の引き数 </summary>
        class C_BackGroundArgument
        {
            /// <summary> 取り込み元ディレクトリ </summary>
            public string dir;
            /// <summary> 審査月(yyMM) </summary>
            public int month;

            public C_BackGroundArgument(string dir, int month)
            {
                this.dir = dir;
                this.month = month;
            }
        }


        /// <summary>バックグラウンド処理</summary>
        /// <param name="sender"></param>
        /// <param name="e">パラメータ： </param>
        /// <see cref="C_BackGroundArgument"/>
        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                C_BackGroundArgument arg = e.Argument as C_BackGroundArgument;


                e.Result = null;
                
                int gid = -1;           // グループID
                int gcnt = -1;          // カウンタ


                // 登録実行
                using (var ins_g = new Npgsql.NpgsqlCommand("INSERT INTO importgroups   (billmonth, scandate) VALUES (:billmonth, :scandate) RETURNING gid", C_ImportGlobal.DBConn))
                using (var ins_r = new Npgsql.NpgsqlCommand("INSERT INTO importreceipts (id, gid, billmonth, fname) VALUES (:id, :gid, :billmonth, :fname)", C_ImportGlobal.DBConn))
                using (var upd_r = new Npgsql.NpgsqlCommand("UPDATE importreceipts SET fname = :fname WHERE id = :id", C_ImportGlobal.DBConn))
                using (var check2 = new Npgsql.NpgsqlCommand("SELECT * FROM receipts WHERE outdata = :id", C_ImportGlobal.DBConn))
                using (var check1 = new Npgsql.NpgsqlCommand("SELECT * FROM importreceipts WHERE id = :id", C_ImportGlobal.DBConn))
                {
                    // パラメータの設定
                    ins_g.Parameters.AddWithValue("billmonth", arg.month);          // グループ、審査月        （パラメータ）
                    ins_g.Parameters.AddWithValue("scandate", DateTime.Now);        // グループ、取込日        （本日の日付）
                    ins_r.Parameters.AddWithValue("billmonth", arg.month);          // 取込画像、審査月    （パラメータ）
                    ins_r.Parameters.Add("id", NpgsqlTypes.NpgsqlDbType.Text);      // 取込画像、電算番号  （TEXT型）
                    ins_r.Parameters.Add("gid", NpgsqlTypes.NpgsqlDbType.Integer);  // 取込画像、グループID（INT型）
                    ins_r.Parameters.Add("fname", NpgsqlTypes.NpgsqlDbType.Text);   // 取込画像、ファイル名（TEXT型）
                    upd_r.Parameters.Add("id", NpgsqlTypes.NpgsqlDbType.Text);      // 取込画像更新、電算番号名（TEXT型）
                    upd_r.Parameters.Add("fname", NpgsqlTypes.NpgsqlDbType.Text);   // 取込画像更新、ファイル名（TEXT型）
                    check1.Parameters.Add("id", NpgsqlTypes.NpgsqlDbType.Text);     // 既存チェック（スキャン）、ファイル名（TEXT型）
                    check2.Parameters.Add("id", NpgsqlTypes.NpgsqlDbType.Text);     // 既存チェック（取り込み）、ファイル名（TEXT型）

                    var files = Directory.GetFiles(arg.dir, "*.*", SearchOption.AllDirectories);
                    
                    for (int i = 0; i < files.Length; i++)
                    {

                        if (m_cancelFlag)
                        {
                            e.Cancel = true;
                            break;
                        }

                        var fromPath = files[i];
                        string fn = Path.GetFileName(fromPath);                     // ファイル名 （拡張子有り）
                        string id = Path.GetFileNameWithoutExtension(fromPath);     // 電算番号   （拡張子なし）

                        backgroundWorker1.ReportProgress(i * 100 / files.Length, fn); 

                        //// ファイル名チェック
                        //if (fromPath == ImportFromDirComment) continue;
                                                
                        // 拡張子チェック
                        string ext = Path.GetExtension(fromPath).ToLower().Trim('.');
                        if (ImportExt.Contains(ext) == false)
                        {
                            //File.Delete(fromPath);
                            continue;
                        }

#if CHK_NUM
                        // 電算番号チェック
                        long nId;
                        if (long.TryParse(id, out nId) == false || nId <= 0)
                        {
                            //File.Delete(fromPath);
                            continue;
                        }

#endif
                        // 既存のスキャン画像をチェック
                        check1.Parameters["id"].Value = id;
                        using (var r = check1.ExecuteReader())
                        {
                            if (r.Read())
                            {
                                // 同一の電算番号が、既にスキャン画像に登録されている ⇒　登録不要（スキャン画像をそのまま使う）
                                //File.Delete(fromPath);
                                continue;
                            }
                        }

                        // 既存の取込画像をチェック
                        int chkGid = -1;
                        check2.Parameters["id"].Value = id;
                        using(var r = check2.ExecuteReader())
                        {
                            if(r.Read())
                            {
                                chkGid = Convert.ToInt32(chkGid);
                            }
                        }
                        if(chkGid>0)
                        {
                            // 同一の電算番号が、既に取込画像に登録されている ⇒　既存の画像を上書きする。
                            string dest = C_ImportGlobal.ReceImportImgPath(chkGid, fn);     // コピー先
                            File.Delete(dest);
                            File.Copy(fromPath, dest);

                            //FileMove(fromPath , C_ImportGlobal.ReceImportImgPath(chkGid, fn));
                            upd_r.Parameters["id"].Value = id;
                            upd_r.Parameters["fname"].Value = fn;
                            upd_r.ExecuteNonQuery();
                            continue;
                        }

                        
                        // 取り込済件数を更新する
                        if (gcnt >= 100 && gid > 0)
                        {
                            C_ImportGlobal.ImportTdata(gid);
                            C_ImportGlobal.UpdateGroupInfo(gid);
                        }
                        
                        if (gid < 0 || gcnt < 0 || gcnt >= 100)
                        {
                            // 新規グループの登録
                            using (var r = ins_g.ExecuteReader())
                            {
                                r.Read();
                                ins_r.Parameters["gid"].Value = gid = Convert.ToInt32(r[0]);
                                gcnt = 0;
                            }
                        }


                        // 取り込み実行
                        string destDir = C_ImportGlobal.ReceImportImgPath(gid, "");
                        string destPath = C_ImportGlobal.ReceImportImgPath(gid, fn);

                        if (Directory.Exists(destDir) == false) Directory.CreateDirectory(destDir);
                        File.Delete(destPath);
                        File.Copy(fromPath, destPath);

                        //File.Delete(fromPath);
                        ins_r.Parameters["id"].Value = id;
                        ins_r.Parameters["fname"].Value = fn;
                        ins_r.ExecuteNonQuery();
                        gcnt++;
                    }
                    if (gid > 0)
                    {                        
                        C_ImportGlobal.ImportTdata(gid);
                        C_ImportGlobal.UpdateGroupInfo(gid);
                    }
                }
            }
            catch (Exception ex)
            {
                e.Result = ex;
            }

        }

        private void backgroundWorker1_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            label1.Text = "取り込み中…\r\n" + e.UserState;
            progressBar1.Value = e.ProgressPercentage;
        }

        private void backgroundWorker1_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            try
            {
                if (e.Cancelled)
                {
                    label1.Text = "取込完了";
                    progressBar1.Value = 0;
                    button1.Text = "取込実行";
                }
                else if ((e.Result as Exception) != null)
                {
                    C_ImportGlobal.OutputErrorLog((e.Result as Exception), false);
                    label1.Text = (e.Result as Exception).Message;
                    progressBar1.Value = 0;
                }
                else
                {
                    this.Close();
                }
            }
            catch (Exception ex)
            {
                C_ImportGlobal.OutputErrorLog(ex, false);
                label1.Text = (e.Result as Exception).Message;
            }
        }
        
        /// <summary>取込実行 or キャンセル</summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                if (backgroundWorker1.IsBusy)
                {
                    m_cancelFlag = true;
                    return;
                }
                else
                {
                    if (Directory.Exists(textBoxPath.Text) == false)
                    {
                        MessageBox.Show("取り込み元を指定して下さい。");
                        return;
                    }

                    int y, m;

                    //20190505164304 furukawa st ////////////////////////
                    //令和1年対応　数字範囲チェック外す

                    if (int.TryParse(textBoxY1.Text, out y) == false )
                    //if (int.TryParse(textBoxY1.Text, out y) == false || y < 20)
                    //20190505164304 furukawa ed ////////////////////////

                    {
                        textBoxY1.Select();
                        textBoxY1.SelectAll();
                        MessageBox.Show("正しい審査月を入力して下さい。");
                        return;
                    }
                    else if (int.TryParse(textBoxM1.Text, out m) == false || m < 1 || m > 12)
                    {
                        textBoxM1.Select();
                        textBoxM1.SelectAll();
                        MessageBox.Show("正しい審査月を入力して下さい。");
                        return;
                    }

                                        
                    m_cancelFlag = false;
                    button1.Text = "キャンセル";
                    label1.Text = "取り込み中…";
                    backgroundWorker1.RunWorkerAsync(new C_BackGroundArgument(textBoxPath.Text, y * 100 + m));
                }

            }
            catch (Exception ex)
            {
                label1.Text = ex.Message;
                C_ImportGlobal.OutputErrorLog(ex, false);
            }
            
        }

        private void FormInport_FormClosing(object sender, FormClosingEventArgs e)
        {
            m_cancelFlag = true;
            if (backgroundWorker1.IsBusy)
            {
                e.Cancel = true;
            }
            else
            {
                e.Cancel = false;
            }
        }

        private void FormInport_DragEnter(object sender, DragEventArgs e)
        {
            // ディレクトリのみドロップ効果をMoveにする
            var path = e.Data.GetData(DataFormats.FileDrop, false) as string[];
            if (path == null) e.Effect = DragDropEffects.None;
            else if (path.Length <= 0) e.Effect = DragDropEffects.None;
            else if (Directory.Exists(path[0]) == false) e.Effect = DragDropEffects.None;
            else e.Effect = DragDropEffects.Copy;

        }

        private void FormInport_DragDrop(object sender, DragEventArgs e)
        {
            var path = e.Data.GetData(DataFormats.FileDrop, false) as string[];
            if (path == null) return;
            if (path.Length <= 0) return;
            if(Directory.Exists(path[0])) textBoxPath.Text = path[0];
        }


        /// <summary> フォルダ名変更時（フォーカスなしに限定） → スクロール位置を変える </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void textBoxPath_TextChanged(object sender, EventArgs e)
        {
            if (textBoxPath.Focused == false)
            {
                textBoxPath.Select(textBoxPath.Text.Length, 0);
            }

            labelPath.Text = "取込元フォルダ：" + System.IO.Path.GetFileName(textBoxPath.Text);

        }


     




    }

}
