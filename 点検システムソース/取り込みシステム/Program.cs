﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace 取り込みシステム
{
    static class Program
    {
        /// <summary>
        /// アプリケーションのメイン エントリ ポイントです。
        /// </summary>
        [STAThread]
        static void Main()
        {
            C_ImportGlobal.OpenDB();
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Form1());
            C_ImportGlobal.CloseDB();
        }
    }
}
